static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#cccccc", "#1a1b26" },
	[SchemeSel] = { "#1c1f24", "#bb9af7" },
	[SchemeSelHighlight] = { "#7aa2f7", "#000000" },
	[SchemeNormHighlight] = { "#7aa2f7", "#000000" },
	[SchemeOut] = { "#000000", "#7aa2f7" },
	[SchemeMid] = { "#d7d7d7", "#1c1f24" },
};
